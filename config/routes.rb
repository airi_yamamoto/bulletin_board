Rails.application.routes.draw do
  root to: 'home#index'

  resources :posts, except: [:edit, :update] do
    get :search, on: :collection
  end

  resources :comments, only: [:create, :destroy]

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions',
    passwords: 'users/passwords',
  }

  devise_scope :user do
    get "sign_in", to: "users/sessions#new"
    delete "sign_out", to: "users/sessions#destroy"
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
