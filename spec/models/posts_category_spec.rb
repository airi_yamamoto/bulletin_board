require 'rails_helper'

RSpec.describe PostsCategory, type: :model do
  let(:posts_category) { build(:posts_category, post: create(:post), category: create(:category)) }

  it "has valid favtory" do
    expect(posts_category).to be_valid
  end

  describe "for relation" do
    it { is_expected.to belong_to(:post) }
    it { is_expected.to belong_to(:category) }
  end

  describe "for validation" do
    it { is_expected.to validate_presence_of :post_id }
    it { is_expected.to validate_presence_of :category_id }
  end
end
