require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build(:user) }

  it "has valid favtory" do
    expect(user).to be_valid
  end

  describe "for relation" do
    it { is_expected.to have_many(:posts) }
    it { is_expected.to have_many(:comments).dependent(:destroy) }
  end

  describe "for validation" do
    it { is_expected.to validate_length_of(:name).is_at_most(20) }
    it { is_expected.to validate_presence_of :email }
    it { is_expected.to validate_uniqueness_of(:email).case_insensitive }

    describe "check email format" do
      context "with valid information" do
        it { is_expected.to allow_value("user@example.com").for(:email) }
        it { is_expected.to allow_value("user+2@EXAMPLE.com").for(:email) }
        it { is_expected.to allow_value("use-r_2@example.co.m").for(:email) }
      end

      context "with invalid information" do
        it { is_expected.not_to allow_value("user@exam").for(:email) }
        it { is_expected.not_to allow_value("user.example.com").for(:email) }
        it { is_expected.not_to allow_value("user@example_com").for(:email) }
        it { is_expected.not_to allow_value("u#s$er@example.com").for(:email) }
        it { is_expected.not_to allow_value("user@example+com").for(:email) }
      end
    end
  end

  describe "for model method" do
    describe "nickname" do
      subject { user_a.nickname }

      let(:user_a) { create(:user, name: name) }

      context "user's name is present" do
        let(:name) { "hello" }

        it { is_expected.to eq name }
      end

      context "user's name is present" do
        let(:name) { "" }

        it { is_expected.to eq "名無しさん" }
      end
    end
  end
end
