require 'rails_helper'

RSpec.describe Post, type: :model do
  describe "for relation" do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to have_many(:comments).dependent(:destroy) }
    it { is_expected.to have_many(:posts_categories).dependent(:destroy) }
    it { is_expected.to have_many(:categories) }
  end

  describe "for validation" do
    let(:post) { build(:post, user: create(:user)) }

    it "has valid factory" do
      expect(post).to be_valid
    end

    it { is_expected.to validate_presence_of :title }
    it { is_expected.to validate_length_of(:title).is_at_most(30) }
    it { is_expected.to validate_presence_of :content }
    it { is_expected.to validate_length_of(:content).is_at_most(1000) }
    it { is_expected.to validate_presence_of :user_id }
  end

  describe "for model scope" do
    describe "from_new_to_old" do
      let!(:old_post) { create(:post, created_at: 1.week.ago) }
      let!(:new_post) { create(:post, created_at: 1.day.ago) }

      let(:sort_by_scope) { Post.from_new_to_old }

      it "sort new_post first" do
        expect(Post.first).to eq old_post
        expect(sort_by_scope.first).to eq new_post
        expect(sort_by_scope.last).to eq old_post
      end
    end

    describe "search_by" do
      let(:search_word) { "sunday!" }
      let(:word) { "bye#{search_word}bye" }

      let(:posts) { [search_by_title, search_by_content, search_by_comment, search_by_both] }

      let!(:other_posts)       { create_list(:post, 5) }
      let!(:search_by_title)   { create(:post, title: word) }
      let!(:search_by_content) { create(:post, content: word) }
      let!(:search_by_comment) { create(:post) }
      let!(:search_by_both)    { create(:post, title: word, content: word) }
      let!(:comments) do
        create_list(:comment, 2, content: word, post: search_by_comment)
        create_list(:comment, 2, content: word, post: search_by_both)
      end

      describe "search by search_word" do
        let(:search_post) { Post.search_by(search_word) }

        it "is not include other_posts" do
          expect(search_post).not_to include(*other_posts)
        end

        it "is search by post title" do
          expect(search_post).to include(search_by_title)
        end

        it "is search by post content" do
          expect(search_post).to include(search_by_content)
        end

        it "is search by post comment" do
          expect(search_post).to include(search_by_comment)
        end

        it "has no duplicate post" do
          expect(search_post).to eq search_post.uniq
          expect(search_post.count).to eq posts.count
        end
      end
    end
  end
end
