require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe "for relation" do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:post) }
  end

  describe "for validation" do
    let(:comment) { build(:comment, user: create(:user), post: create(:post)) }

    it "has valid factory" do
      expect(comment).to be_valid
    end

    it { is_expected.to validate_presence_of :content }
    it { is_expected.to validate_length_of(:content).is_at_most(1000) }
    it { is_expected.to validate_presence_of :user_id }
    it { is_expected.to validate_presence_of :post_id }
  end
end
