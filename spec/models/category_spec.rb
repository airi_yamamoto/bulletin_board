require 'rails_helper'

RSpec.describe Category, type: :model do
  let(:category) { build(:category) }

  it "has valid favtory" do
    expect(category).to be_valid
  end

  describe "for relation" do
    it { is_expected.to belong_to(:parent).optional }
    it { is_expected.to have_many(:children).dependent(:destroy) }
    it { is_expected.to have_many(:posts_categories).dependent(:destroy) }
    it { is_expected.to have_many(:posts) }
  end

  describe "for validation" do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_length_of(:name).is_at_most(10) }
  end
end
