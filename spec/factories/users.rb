FactoryBot.define do
  factory :user do
    name     { "example" }
    password { "password" }
    sequence(:email) { |n| "user_#{n}@example.com" }
  end
end
