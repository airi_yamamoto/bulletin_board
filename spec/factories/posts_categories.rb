FactoryBot.define do
  factory :posts_category do
    association :post
    association :category
  end
end
