FactoryBot.define do
  factory :post do
    sequence(:title) { |n| "Title_#{n}" }
    content { "Hello world!" }
    association :user
  end
end
