FactoryBot.define do
  factory :comment do
    sequence(:content) { |n| "Comment_#{n}" }
    association :user
    association :post
  end
end
