FactoryBot.define do
  factory :category, aliases: [:parent] do
    sequence(:name) { |n| "Category_#{n}" }
    parent_id { nil }
  end
end
