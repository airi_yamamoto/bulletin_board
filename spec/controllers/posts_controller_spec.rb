require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  subject { response }

  let(:user) { create(:user) }

  before { sign_in user }

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #new" do
    context "current_user is present" do
      before { get :new }

      describe "for response" do
        it { is_expected.to have_http_status(:success) }
        it { is_expected.to render_template("posts/new") }
      end

      describe "for instance variable" do
        it "assigns @post" do
          expect(assigns(:post)).to have_attributes(id: nil, user_id: user.id)
          expect(assigns(:post).instance_of?(Post)).to be true
        end
      end
    end

    context "when current_user is not present" do
      before do
        sign_out user
        get :new
      end

      it { is_expected.to redirect_to new_user_session_path }
    end
  end

  describe "POST create" do
    context "when current_user is present" do
      describe "for instance variable" do
        let(:post_attributes) { { title: "title", user_id: user.id } }

        before { post :create, params: { post: post_attributes } }

        it "assigns @post" do
          expect(assigns(:post)).to have_attributes post_attributes
        end
      end

      context "with valid information" do
        it "create new post and redirect_to post_path" do
          expect do
            post :create, params: { post: attributes_for(:post) }
          end.to change { user.posts.count }.by(1)
          is_expected.to redirect_to post_path(assigns(:post).id)
        end
      end

      context "with invalid information" do
        it "does not change post count and render new" do
          expect do
            post :create, params: { post: attributes_for(:post, title: nil) }
          end.not_to change { user.posts.count }
          is_expected.to render_template("posts/new")
        end
      end
    end

    context "when current_user is not present" do
      before { sign_out user }

      it "redirect_to new_user_session_path" do
        expect do
          post :create, params: { post: attributes_for(:post) }
        end.not_to change(Post, :count)
        is_expected.to redirect_to new_user_session_path
      end
    end
  end

  describe "GET #show" do
    let!(:post)           { create(:post) }
    let!(:comments)       { create_list(:comment, 40, post: post) }
    let!(:other_comments) { create_list(:comment, 5) }

    before { get :show, params: { id: post.id } }

    describe "for response" do
      it { is_expected.to have_http_status(:success) }
      it { is_expected.to render_template("posts/show") }
    end

    describe "for instance variable" do
      it "assigns @post" do
        expect(assigns(:post)).to eq post
      end

      it "assigns @comment" do
        expect(assigns(:comment)).to have_attributes(user_id: user.id)
        expect(assigns(:comment).instance_of?(Comment)).to be true
      end

      it "assigns @comments" do
        expect(assigns(:comments).count).to eq Comment.default_per_page
        expect(comments).to include(*assigns(:comments))
        expect(assigns(:comments)).not_to include(*other_comments)
      end
    end
  end

  describe "DELETE destroy" do
    let!(:post) { create(:post, user: user) }
    let!(:other_post) { create(:post) }

    context "post's user is current_user" do
      it "delete the post and redirect to root_path" do
        expect do
          delete :destroy, params: { id: post.id }
        end.to change(Post, :count).by(-1)
        is_expected.to redirect_to root_path
      end
    end

    context "post's user is not current_user" do
      it "does not delete the post and redirect to root_path" do
        expect do
          delete :destroy, params: { id: other_post.id }
        end.not_to change(Post, :count)
        is_expected.to redirect_to root_path
      end
    end

    context "current_user is not present" do
      it "redirect to new_user_session_path" do
        sign_out user
        expect do
          delete :destroy, params: { id: post.id }
        end.not_to change(Post, :count)
        is_expected.to redirect_to new_user_session_path
      end
    end
  end

  describe "GET #search" do
    let(:word)   { "word!!!" }

    before { get :search, params: { search: word } }

    describe "for response" do
      it { is_expected.to have_http_status(:success) }
      it { is_expected.to render_template("posts/search") }
    end

    describe "for instance variable" do
      let(:search_posts) { [*posts, new_post] }

      let!(:new_post) { create(:post, title: word, created_at: Time.current) }
      let!(:posts)    { create_list(:post, 50, title: word, created_at: 1.day.ago) }
      let!(:other)    { create(:post) }

      it "assigns @search_word" do
        expect(assigns(:search_word)).to eq word
      end

      describe "assigns @posts" do
        it "has the number of posts set by kaminari" do
          expect(assigns(:posts).count).to eq Post.default_per_page
        end

        it "is included in posts" do
          expect(search_posts).to include(*assigns(:posts))
        end

        it "does not have other" do
          expect(assigns(:posts)).not_to include(other)
        end

        it "return posts from new to old" do
          expect(assigns(:posts).first).to eq new_post
        end
      end
    end
  end
end
