require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  subject { response }

  let(:user)   { create(:user) }
  let(:thread) { create(:post) }

  before { sign_in user }

  describe "POST create" do
    let(:comment_attributes) { { content: content, post_id: thread.id } }

    context "when current_user is not present" do
      let(:content) { "hello" }

      before { sign_out user }

      it "redirect_to new_user_session_path" do
        expect do
          post :create, params: { comment: comment_attributes }
        end.not_to change(Comment, :count)
        is_expected.to redirect_to new_user_session_path
      end
    end

    context "when current_user is present" do
      context "with valid information" do
        let(:content) { "hello" }

        it "create new comment and redirect_to post_path(thread)" do
          expect do
            post :create, params: { comment: comment_attributes }
          end.to change { user.comments.count }.by(1)

          is_expected.to redirect_to post_path(thread)
        end

        describe "for instance variable" do
          before { post :create, params: { comment: comment_attributes } }

          it "assigns @comment" do
            expect(assigns(:comment)).to have_attributes comment_attributes
          end
        end
      end

      context "with invalid information" do
        let(:content) { "" }

        it "does not change comment count and render render post/show" do
          expect do
            post :create, params: { comment: comment_attributes }
          end.not_to change { user.comments.count }
          is_expected.to render_template "posts/show"
        end

        describe "for instance variable" do
          before { post :create, params: { comment: comment_attributes } }

          it "assigns @comment" do
            expect(assigns(:comment)).to have_attributes comment_attributes
          end

          it "assigns @post" do
            expect(assigns(:post)).to eq thread
          end
        end
      end
    end
  end

  describe "DELETE destroy" do
    let!(:comment)       { create(:comment, user: user) }
    let!(:other_comment) { create(:comment) }

    context "current_user is not present" do
      before { sign_out user }

      it "redirect_to new_user_session_path" do
        expect do
          delete :destroy, params: { id: comment.id }
        end.not_to change(Comment, :count)

        is_expected.to redirect_to new_user_session_path
      end
    end

    context "comment's user is current_user" do
      context "when there is a previous URL" do
        let(:previous_url) { post_path(thread.id) }

        it "delete the post and redirect to root_path" do
          from previous_url

          expect do
            delete :destroy, params: { id: comment.id }
          end.to change { user.comments.count }.by(-1)

          is_expected.to redirect_to previous_url
        end
      end

      context "when there is a previous URL" do
        it "delete the post and redirect to root_path" do
          expect do
            delete :destroy, params: { id: comment.id }
          end.to change { user.comments.count }.by(-1)

          is_expected.to redirect_to root_path
        end
      end
    end

    context "comment's user is not current_user" do
      context "when there is a previous URL" do
        let(:previous_url) { post_path(thread.id) }

        it "does not delete the post and redirect to back" do
          from previous_url

          expect do
            delete :destroy, params: { id: other_comment.id }
          end.not_to change(Comment, :count)

          is_expected.to redirect_to previous_url
        end
      end

      context "when there is a previous URL" do
        it "does not delete the post and redirect to root_path" do
          expect do
            delete :destroy, params: { id: other_comment.id }
          end.not_to change(Comment, :count)

          is_expected.to redirect_to root_path
        end
      end
    end
  end
end
