require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  describe "GET #index" do
    subject { response }

    let(:page_number)  { 1 }

    let!(:new_post) { create(:post, created_at: Time.current) }
    let!(:old_post) { create(:post, created_at: 1.year.ago) }
    let!(:posts)    { create_list(:post, 30, created_at: 1.week.ago) }

    before { get :index, params: { page: page_number } }

    describe "for response" do
      it { is_expected.to have_http_status(:success) }
      it { is_expected.to render_template("home/index") }
    end

    describe "for instance variable" do
      context "assigns @post" do
        it { expect(assigns(:posts).count).to eq Post.default_per_page }

        it "sorted by newest" do
          expect(assigns(:posts).first).to eq new_post
          expect(assigns(:posts)).not_to include(old_post)
        end
      end
    end
  end
end
