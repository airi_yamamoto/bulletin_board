module RedirectBack
  def from(url)
    request.env['HTTP_REFERER'] = url
  end
end

RSpec.configure do |config|
  config.include RedirectBack
end
