module I18nTranslate
  def t(*word)
    I18n.translate!(*word)
  end
end

RSpec.configure do |config|
  config.include I18nTranslate, type: :feature
end
