require 'rails_helper'

RSpec.feature "SignUp", type: :feature do
  subject { page }

  let(:user) { build(:user) }

  let(:sign_up_ja)         { t("devise.registrations.new.sign_up") }
  let(:email_field)        { t("activerecord.attributes.user.email") }
  let(:password_field)     { t("activerecord.attributes.user.password") }
  let(:confirmation_field) { t("activerecord.attributes.user.password_confirmation") }

  before { visit new_user_registration_path }

  it { is_expected.to have_title full_title(sign_up_ja) }

  it "checks sign in process" do
    submit_with_empty_information
    is_expected.to have_content "エラーが発生"

    submit_with_information(email: "user@user")
    is_expected.to have_content "エラーが発生"

    submit_with_information(email: user.email)

    expect(current_path).to eq root_path
    is_expected.to have_css ".alert-notice"
  end

  def submit_with_information(email:)
    fill_in email_field, with: email
    fill_in password_field, with: "password"
    fill_in confirmation_field, with: "password"

    click_button sign_up_ja
  end

  def submit_with_empty_information
    fill_in email_field, with: ""
    fill_in password_field, with: ""
    fill_in confirmation_field, with: ""

    click_button sign_up_ja
  end
end
