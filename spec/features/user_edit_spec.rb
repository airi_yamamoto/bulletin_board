require 'rails_helper'

RSpec.feature "UserEdit", type: :feature do
  subject { page }

  let(:user) { create(:user, name: "before") }

  let(:page_title) { t('devise.registrations.edit.title', resource: User.model_name.human) }

  let(:name_field)             { t("activerecord.attributes.user.name") }
  let(:email_field)            { t("activerecord.attributes.user.email") }
  let(:current_password_field) { t("activerecord.attributes.user.current_password") }
  let(:submit)                 { t('devise.registrations.edit.update') }

  before do
    sign_in user
    visit edit_user_registration_path
  end

  it { is_expected.to have_title full_title(page_title) }

  describe "check update user information" do
    let(:new_name) { "a" * 20 }

    it "change user name when information is valid" do
      expect do
        fill_in current_password_field, with: user.password
        fill_in name_field, with: "a" * 21
        click_button submit
      end.not_to change { user.reload.name }

      is_expected.to have_content "エラーが発生"

      expect do
        submit_with_information(name: new_name)
      end.to change { user.reload.name }.from("before").to(new_name)

      expect(current_path).to eq root_path
      is_expected.to have_selector ".alert-notice"
    end

    def submit_with_information(name:)
      fill_in current_password_field, with: user.password
      fill_in name_field, with: name
      click_button submit
    end
  end
end
