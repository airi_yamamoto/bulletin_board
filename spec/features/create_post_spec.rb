require 'rails_helper'

RSpec.feature "CreatePosts", type: :feature do
  subject { page }

  let(:user)      { create(:user) }
  let(:last_post) { user.posts.last }

  it "create post process" do
    sign_in user
    visit root_path
    click_on "投稿"

    expect(current_path).to eq new_post_path
    is_expected.to have_title full_title("スレッド投稿")

    submit_with_empty_information
    within "#errors_explanation" do
      is_expected.to have_content "エラーがあります"
    end

    expect do
      submit_with_valid_information
    end.to change { user.posts.count }.by(1)

    expect(current_path).to eq post_path(last_post.id)
    is_expected.to have_title full_title(last_post.title)
    within ".alert-success" do
      is_expected.to have_content "スレッドを作成しました"
    end
  end

  def submit_with_empty_information
    post_attribute = "activerecord.attributes.post"
    fill_in t("#{post_attribute}.title"), with: ""
    fill_in t("#{post_attribute}.content"), with: ""
    within ".actions" do
      click_on "投稿"
    end
  end

  def submit_with_valid_information
    post_attribute = "activerecord.attributes.post"
    fill_in t("#{post_attribute}.title"), with: "Hello"
    fill_in t("#{post_attribute}.content"), with: "World!!!"
    within ".actions" do
      click_on "投稿"
    end
  end
end
