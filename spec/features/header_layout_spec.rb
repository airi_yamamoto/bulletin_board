require 'rails_helper'

RSpec.feature "HeaderLayouts", type: :feature do
  subject { page }

  before { visit root_path }

  describe "header layouts" do
    it { is_expected.to have_link "Yama_ch", href: root_path }

    context "about registrations links" do
      let(:user) { create(:user) }

      it "Link changes depending on user login status" do
        is_expected.to have_link "ログイン"
        is_expected.to have_no_link "ログアウト"
        is_expected.to have_no_link "ユーザ編集"
        is_expected.to have_no_link "投稿"

        sign_in user
        visit root_path

        is_expected.to have_no_link "ログイン"
        is_expected.to have_link "ログアウト"
        is_expected.to have_link "ユーザ編集"
        is_expected.to have_link "投稿"
      end
    end

    context "about search form" do
      it "has search form" do
        is_expected.to have_selector "form.search_form"
      end

      context "when searching for posts" do
        let(:search_word) { "hello" }

        it "has correct title" do
          submit_search_form_with(word: search_word)
          is_expected.to have_title(full_title(search_word))
        end

        context "when less than 30 posts searched" do
          let(:word)        { "bye#{search_word}bye" }

          let(:posts)     { [post_a, post_b, post_c] }
          let!(:others)   { create_list(:post, 5, content: "other") }
          let!(:post_a)   { create(:post, title: word) }
          let!(:post_b)   { create(:post, content: word) }
          let!(:post_c)   { create(:post) }
          let!(:comments) { create_list(:comment, 2, content: word, post: post_c) }

          it "returns all posts with search_word" do
            submit_search_form_with(word: search_word)

            within(".search_word") { is_expected.to have_content search_word }

            within ".threads_field" do
              expect_to_display_posts_details(posts: posts)
              expect_not_to_display_posts_details(posts: others)
            end
          end
        end

        context "when more than 31 posts searched" do
          let!(:posts) { create_list(:post, 35, content: "bye#{search_word}bye") }

          it "returns posts with pagination" do
            submit_search_form_with(word: search_word)

            within(".search_word") { is_expected.to have_content search_word }

            within ".threads_field" do
              is_expected.to have_selector ".post", count: Post.default_per_page
              is_expected.to have_selector ".pagination", count: 2
            end
          end
        end

        context "when no thread searched" do
          let!(:posts) { create_list(:post, 30, title: "bye", content: "bye") }

          it "returns no post" do
            submit_search_form_with(word: search_word)

            within(".search_word") { is_expected.to have_content search_word }

            is_expected.to have_no_selector ".threads_field"
            is_expected.to have_content "スレッドが見つかりません"
          end
        end

        def submit_search_form_with(word:)
          fill_in "search", with: word
          click_on "検索"
          expect(current_path).to eq search_posts_path
        end

        def expect_to_display_posts_details(posts:)
          posts.each do |post|
            within "#post_#{post.id}" do
              is_expected.to have_content post.title
              is_expected.to have_content post.content
            end
          end
        end

        def expect_not_to_display_posts_details(posts:)
          posts.each do |post|
            is_expected.to have_no_selector "#post_#{post.id}"
          end
        end
      end
    end
  end
end
