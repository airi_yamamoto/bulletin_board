require 'rails_helper'

RSpec.feature "HomepageLayouts", type: :feature do
  subject { page }

  let!(:posts)    { create_list(:post, 29, created_at: 1.week.ago) }
  let!(:new_post) { create(:post, title: "NEW", created_at: 1.day.ago) }
  let!(:old_post) { create(:post, title: "OLD", created_at: 1.year.ago) }

  before { visit root_path }

  describe "check layout" do
    it { is_expected.to have_selector ".post", count: Post.default_per_page }

    it "has pagination" do
      is_expected.to have_selector ".pagination", count: 2
    end

    it "sort new_post first" do
      within first(".card") do
        expect_to_have_post_details(post: new_post)
      end
    end

    it "has posts details" do
      posts.each do |post|
        within "#post_#{post.id}" do
          expect_to_have_post_details(post: post)
        end
      end
    end
  end

  def expect_to_have_post_details(post:)
    within ".card-body" do
      is_expected.to have_content post.title
      is_expected.to have_content post.content
    end

    within ".card-text" do
      is_expected.to have_content post.user.nickname
      is_expected.to have_content post.comments.count
      is_expected.to have_content post.created_at
      is_expected.to have_link nil, href: post_path(post)
    end
  end
end
