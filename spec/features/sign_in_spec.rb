require 'rails_helper'

RSpec.feature "SignIn", type: :feature do
  subject { page }

  let(:user) { create(:user) }

  let(:sign_in_ja)         { t("devise.sessions.new.sign_in") }
  let(:email_field)        { t("activerecord.attributes.user.email") }
  let(:password_field)     { t("activerecord.attributes.user.password") }
  let(:confirmation_field) { t("activerecord.attributes.user.password_confirmation") }

  before { visit new_user_session_path }

  it { is_expected.to have_title full_title(sign_in_ja) }

  it "checks sign in process" do
    fill_in email_field, with: user.email
    fill_in password_field, with: "invalid"
    click_button sign_in_ja

    is_expected.to have_content "メールアドレスまたはパスワードが違います"

    fill_in email_field, with: user.email
    fill_in password_field, with: user.password
    click_button sign_in_ja

    expect(current_path).to eq root_path
    is_expected.to have_css ".alert-notice"
  end
end
