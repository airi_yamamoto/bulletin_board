require 'rails_helper'

RSpec.feature "ShowPosts", type: :feature do
  subject { page }

  let(:comment_per_page) { Comment.default_per_page }

  let(:user)       { create(:user) }
  let(:other_user) { create(:user) }

  let!(:post) { create(:post, user: user) }

  let!(:comments)       { create_list(:comment, 40, post: post, user: user) }
  let!(:other_comments) { create_list(:comment, 10) }

  let(:first_page_comments) { comments.first(comment_per_page) }

  describe "check layout" do
    before { visit post_path(post) }

    it { is_expected.to have_title full_title(post.title) }

    it "has details information of the post" do
      within ".main_thread" do
        is_expected.to have_content post.title
        is_expected.to have_content post.content
        is_expected.to have_content post.created_at
        is_expected.to have_content post.comments.count
        is_expected.to have_content user.nickname
      end
    end

    describe "check comments layout" do
      it { is_expected.to have_selector ".pagination", count: 2 }
      it { is_expected.to have_selector ".comment-card", count: comment_per_page }

      it "has comments details" do
        first_page_comments.each do |comment|
          within "#comment_#{comment.id}" do
            is_expected.to have_content comment.content
            is_expected.to have_content comment.created_at
            is_expected.to have_content comment.user.nickname
          end
        end
      end

      it "does not have other comments details" do
        other_comments.each do |othre_comment|
          is_expected.to have_no_selector "#comment_#{othre_comment.id}"
        end
      end

      it "has correct comment number" do
        within first(".comment-card") do
          within(".comment_number") { is_expected.to have_content "1" }
        end

        first(".pagination").click_on "次"

        within first(".comment-card") do
          within(".comment_number") { is_expected.to have_content "#{comment_per_page + 1}" }
        end
      end
    end
  end

  context "when current_user is not present" do
    before { visit post_path(post) }

    it "has no link to delete the post" do
      is_expected.to have_no_link "スレッドを削除"
    end

    it "has no form to post comment" do
      is_expected.to have_no_selector ".comment_form"
    end
  end

  describe "for link to delete data" do
    shared_examples "there_is_no_link_to_delete_data" do
      it "has no link to delete the post" do
        within("#post_#{post.id}") { is_expected.to have_no_link "スレッドを削除" }
      end

      it "has a link to delete the comment and redirect_back" do
        within(".comments_field") { is_expected.to have_no_link "削除" }
      end
    end

    context "when current_user is not present" do
      before { visit post_path(post) }

      it_behaves_like "there_is_no_link_to_delete_data"
    end

    context "when current_user is present" do
      context "when current_user and post's user, comment's user are the same" do
        before { sign_in_and_visit_post_path(user) }

        it "has a link to delete the post and redirect to root_path" do
          within "#post_#{post.id}" do
            expect { click_on "スレッドを削除" }.to change { user.posts.count }.by(-1)
          end
          expect(current_path).to eq root_path
        end

        it "has a link to delete the comment and redirect back" do
          within first(".comment-card") do
            expect { click_on "削除" }.to change { user.comments.count }.by(-1)
          end
          expect(current_path).to eq post_path(post)
        end
      end

      context "when current_user and post's user, comment's are different" do
        before { sign_in_and_visit_post_path(other_user) }

        it_behaves_like "there_is_no_link_to_delete_data"
      end
    end
  end

  describe "create comment" do
    let(:latest_comment) { Comment.last }
    let(:word)           { "Hello!!" }

    it "create comments" do
      sign_in_and_visit_post_path(user)

      submit_with_comment(content: "")
      is_expected.to have_content "エラー"

      submit_with_comment(content: "a" * 1001)
      is_expected.to have_content "エラー"

      expect do
        submit_with_comment(content: word)
      end.to change { user.comments.count }.by(1)

      expect(latest_comment).to have_attributes(content: word, post_id: post.id, user_id: user.id)

      is_expected.to have_content "投稿しました"
      expect(current_path).to eq post_path(post)
    end

    def submit_with_comment(content:)
      fill_in "comment_content", with: content
      click_button "投稿"
    end
  end

  def sign_in_and_visit_post_path(user)
    sign_in user
    visit post_path(post)
  end
end
