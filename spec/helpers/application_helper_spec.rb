require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "about full_title hepler" do
    let(:base_title) { "Yama ch" }

    context "when page_title is not empty" do
      it "returns base_title with page_title" do
        expect(helper.full_title("HELLO")).to eq "HELLO - #{base_title}"
      end
    end

    context "when page_title is empty" do
      it "returns only base_title" do
        expect(helper.full_title).to eq base_title
      end
    end
  end

  describe "about current_user? helper" do
    let(:user)       { create(:user) }
    let(:other_user) { create(:user) }

    context "user and current_user are the same" do
      it "returns true" do
        sign_in user
        expect(helper.current_user?(user)).to be true
      end
    end

    context "user and current_user are different" do
      it "returns true" do
        sign_in user
        expect(helper.current_user?(other_user)).to be false
      end
    end
  end

  describe "about current_path? helper" do
    let(:post)    { create(:post) }
    let(:path)    { post_path(post) }
    let(:request) { double('request', path: path) }

    before  { allow(helper).to receive(:request).and_return(request) }

    context "current_path and expect_path are the same" do
      it "returns true" do
        expect(helper.current_path?(path)).to be true
      end
    end

    context "current_path and expect_path are different" do
      let(:other_path) { root_path }

      it "returns true" do
        expect(helper.current_path?(other_path)).to be false
      end
    end
  end
end
