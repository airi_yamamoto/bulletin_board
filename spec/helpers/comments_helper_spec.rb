require 'rails_helper'

RSpec.describe CommentsHelper, type: :helper do
  describe "comment_number" do
    let(:per)    { 5 }
    let(:index)  { 2 }

    let(:comment_number_on_page) { index + 1 }
    let(:correct_number) { comment_number_on_page + ((page.to_i - 1) * per) }

    before { stub_const("#{Comment}::COMMENT_PAGENATE_PER", per) }

    context "when page is 1 or more" do
      let(:page) { "2" }

      it "return the correct_number" do
        expect(helper.comment_number(index: index, page: page)).to eq correct_number
      end
    end

    shared_examples "comment_number_on_page" do
      it { expect(helper.comment_number(index: index, page: page)).to eq comment_number_on_page }
    end

    context "when page is 0 or less" do
      let(:page) { "0" }

      it_behaves_like "comment_number_on_page"
    end

    context "when page number is character" do
      let(:page) { "hello" }

      it_behaves_like "comment_number_on_page"
    end

    context "there is no page" do
      let(:page)   { nil }

      it_behaves_like "comment_number_on_page"
    end
  end
end
