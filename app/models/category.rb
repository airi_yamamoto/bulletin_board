class Category < ApplicationRecord
  belongs_to :parent, class_name: :Category, optional: true
  has_many :children, class_name: :Category,
                      foreign_key: :parent_id,
                      dependent: :destroy

  has_many :posts_categories, dependent: :destroy
  has_many :posts,            through: :posts_categories

  validates :name,  presence: true,
                    length: { maximum: 10 }
end
