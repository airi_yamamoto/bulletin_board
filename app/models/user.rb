class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :posts
  has_many :comments, dependent: :destroy

  VALID_EMAIL_REGEX = /\A+[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze

  validates :name,  length: { maximum: 20 }
  validates :email, format: { with: VALID_EMAIL_REGEX }

  def nickname
    if name.blank?
      "名無しさん"
    else
      name
    end
  end
end
