class Post < ApplicationRecord
  paginates_per 30

  belongs_to :user
  has_many :comments,         dependent: :destroy
  has_many :posts_categories, dependent: :destroy
  has_many :categories,       through: :posts_categories

  validates :title,   presence: true, length: { maximum: 30 }
  validates :content, presence: true, length: { maximum: 1000 }
  validates :user_id, presence: true

  scope :from_new_to_old, -> { includes(:user).order(created_at: :desc) }

  scope :search_by, -> (word) do
    eager_load(:comments).where(
      "posts.title LIKE :word
        OR posts.content LIKE :word
        OR comments.content LIKE :word",
      word: "%#{sanitize_sql_like(word)}%",
    )
  end
end
