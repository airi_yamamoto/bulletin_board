module ApplicationHelper
  def full_title(page_title = "")
    base_title = "Yama ch"
    if page_title.blank?
      base_title
    else
      "#{page_title} - #{base_title}"
    end
  end

  def current_user?(user)
    !!(current_user == user)
  end

  def current_path?(path)
    !!(request.path == path)
  end
end
