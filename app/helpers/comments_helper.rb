module CommentsHelper
  # インデックスの値と、前のページまでの総コメント数から、コメント番号を計算する
  def comment_number(index:, page:)
    comment_number_on_page = index + 1

    if page && page.to_i >= 1
      number_of_previous_pages = page.to_i - 1
      number_of_previous_comments = Comment::COMMENT_PAGENATE_PER * number_of_previous_pages

      comment_number_on_page + number_of_previous_comments
    else
      comment_number_on_page
    end
  end
end
