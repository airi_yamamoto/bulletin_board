class HomeController < ApplicationController
  def index
    @posts = Post.from_new_to_old.page(params[:page])
  end
end
