class PostsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :destroy]

  def index
  end

  def new
    @post = current_user.posts.build
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = "スレッドを作成しました！"
      redirect_to @post
    else
      render "new"
    end
  end

  def show
    @post = Post.find(params[:id])
    @comments = @post.comments.includes(:user).page(params[:page])
    if current_user.present?
      @comment = current_user.comments.build
    end
  end

  def destroy
    post = current_user.posts.find_by(id: params[:id])
    if post
      post.destroy
      flash[:success] = "スレッドを削除しました。"
    end
    redirect_to root_path
  end

  def search
    @search_word = params[:search]
    @posts = Post.search_by(@search_word).from_new_to_old.page(params[:page])
  end

  private

  def post_params
    params.require(:post).permit(:title, :content)
  end
end
