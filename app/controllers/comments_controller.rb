class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @comment = current_user.comments.build(comment_params)
    if @comment.save
      flash[:success] = "コメントを投稿しました。"
      redirect_to @comment.post
    else
      @post = Post.find_by(id: @comment.post_id)
      @comments = @post.comments.includes(:user).page(params[:page])
      render template: "posts/show"
    end
  end

  def destroy
    comment = current_user.comments.find_by(id: params[:id])
    comment.destroy if comment
    redirect_back(fallback_location: root_path)
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :post_id)
  end
end
