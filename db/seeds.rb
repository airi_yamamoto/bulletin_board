# User create

password = "password"

User.create!({
  name: "test_user",
  email: "user@example.com",
  password: password,
  password_confirmation: password,
})

5.times do |n|
  name  = Faker::Name.name
  email = "user_#{n + 1}@example.com"
  User.create!({
    name: name,
    email: email,
    password: password,
    password_confirmation: password,
  })
end

# Post and Comment crete

first_user = User.first
users = User.order(:created_at).take(5)

## Post create
first_user.posts.create!({
  title: "First thread!!",
  content: Faker::Lorem.sentence(word_count: 5),
})

users.each do |user|
  10.times do
    title = Faker::Lorem.word
    content = Faker::Lorem.paragraphs(number: 2).join

    user.posts.create!(title: title, content: content)
  end
end

## Comment Create
first_post = Post.first
posts = Post.order(created_at: :desc).take(10)

first_user.comments.create!({
  post_id: first_post.id,
  content: Faker::Lorem.paragraphs(number: 1).join,
})

10.times do
  posts.each do |post|
    content = Faker::Lorem.paragraphs(number: 1).join
    users.each { |user| user.comments.create!(content: content, post_id: post.id) }
  end
end

# Category

Category.create!(name: "速報")
parent_a = Category.find_by(name: "速報")
parent_a.children.create!(name: "政治")
parent_a.children.create!(name: "地震")
parent_a.children.create!(name: "事件")

Category.create!(name: "人間関係")
parent_b = Category.find_by(name: "人間関係")
parent_b.children.create!(name: "最近のニュース")
parent_b.children.create!(name: "恋愛")
parent_b.children.create!(name: "仕事")

Category.create!(name: "食事")
parent_c = Category.find_by(name: "食事")
parent_c.children.create!(name: "おすすめ")
parent_c.children.create!(name: "スイーツ")
parent_c.children.create!(name: "お肉")
parent_c.children.create!(name: "レストラン")

Category.create!(name: "その他")

# PostsCategory

posts = Post.order(created_at: :desc).take(10)
categories = Category.all.reject { |category| category.children.present? }

## Create first PostsCategory
first_post = posts.first
first_category = categories.first

first_post.posts_categories.create!(category: first_category)

## Create PostsCategories
posts.each do |post|
  post_categories = categories.sample(2)

  post_categories.each do |post_category|
    post.posts_categories.create(category: post_category)
  end
end
